package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.telecomnancy.Command;
import eu.telecomnancy.Invoker;
import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class SensorView extends JPanel implements Observer {
    private AbstractSensor sensor;

    private JLabel value = new JLabel("N/A �C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    
    private Invoker invoker = new Invoker();
    
    private Properties commands;

    public SensorView(AbstractSensor c) {
    	
    	try{
    		ReadPropertyFile rp=new ReadPropertyFile();
            commands = rp.readFile("/eu/telecomnancy/commande.properties");
    	} catch (IOException e){
    		e.printStackTrace();
    	}
    	

        this.sensor = c;
        this.sensor.addObserver(this);
        
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
        	@Override
            public void actionPerformed(ActionEvent e) {
        		try {
					Command command = (Command) Class.forName(commands.getProperty("commandeOn"))
										.getConstructor(AbstractSensor.class)
										.newInstance(sensor);
					invoker.setCommand(command);
	                invoker.execute();
				} catch (InstantiationException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					e1.printStackTrace();
				} catch (NoSuchMethodException e1) {
					e1.printStackTrace();
				} catch (SecurityException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	try {
					Command command = (Command) Class.forName(commands.getProperty("commandeOff"))
										.getConstructor(AbstractSensor.class)
										.newInstance(sensor);
					invoker.setCommand(command);
	                invoker.execute();
				} catch (InstantiationException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					e1.printStackTrace();
				} catch (NoSuchMethodException e1) {
					e1.printStackTrace();
				} catch (SecurityException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	try {
					Command command = (Command) Class.forName(commands.getProperty("commandeUpdate"))
										.getConstructor(AbstractSensor.class)
										.newInstance(sensor);
					invoker.setCommand(command);
	                invoker.execute();
				} catch (InstantiationException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					e1.printStackTrace();
				} catch (NoSuchMethodException e1) {
					e1.printStackTrace();
				} catch (SecurityException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		ISensor sensor = (ISensor) arg0;
		try{
			this.value.setText("" + sensor.getValue() + " �C");
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
	}
}
