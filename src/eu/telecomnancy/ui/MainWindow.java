package eu.telecomnancy.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import eu.telecomnancy.sensor.AbstractSensor;

public class MainWindow extends JFrame {

    private AbstractSensor sensor;
    private SensorView sensorView;

    public MainWindow(AbstractSensor sensor) {
        this.sensor = sensor;
        this.sensorView = new SensorView(this.sensor);

        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


}
