package eu.telecomnancy.ui;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.RoundDecorator;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.ToFarenheitDecorator;


public class ConsoleUI {

	private ISensor originalSensor;
    private ISensor sensor;
    private Scanner console;
    private boolean isFarenheit;
    private boolean isRounded;

    public ConsoleUI(ISensor sensor) {
    	this.originalSensor = sensor;
        this.sensor = sensor;
        this.console = new Scanner(System.in);
        manageCLI();
    }

    public void manageCLI() {
        String rep = "";
        System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value - convert|c: farenheit/celsius - round|r: round/unround");
        while (!"q".equals(rep)) {
            try {
                System.out.print(":> ");
                rep = this.console.nextLine();
                if ("on".equals(rep) || "o".equals(rep)) {
                    this.sensor.on();
                    System.out.println("sensor turned on.");
                } else if ("off".equals(rep) || "O".equals(rep)) {
                    this.sensor.off();
                    System.out.println("sensor turned off.");
                } else if ("status".equals(rep) || "s".equals(rep)) {
                    System.out.println("status: " + this.sensor.getStatus());
                } else if ("update".equals(rep) || "u".equals(rep)) {
                    this.sensor.update();
                    System.out.println("sensor value refreshed.");
                } else if ("value".equals(rep) || "v".equals(rep)) {
                    System.out.println("value: " + this.sensor.getValue());
                } else if ("convert".equals(rep) || "c".equals(rep)) {
                	if(this.isFarenheit){
                		if(this.isRounded){
                			this.sensor = new RoundDecorator(this.originalSensor);
                		} else {
                			this.sensor = this.originalSensor;
                		}
                		this.isFarenheit = false;
                	} else {
                		if(this.isRounded){
                			this.sensor = new RoundDecorator(new ToFarenheitDecorator(this.originalSensor));
                		} else {
                			this.sensor = new ToFarenheitDecorator(this.originalSensor);
                		}
                		this.isFarenheit = true;
                	}
                } else if ("round".equals(rep) || "r".equals(rep)) {
                	if(this.isRounded){
                		if(this.isFarenheit){
                			this.sensor = new ToFarenheitDecorator(this.originalSensor);
                		} else {
                			this.sensor = this.originalSensor;
                		}
                		this.isRounded = false;
                	} else {
                		this.sensor = new RoundDecorator(this.sensor);
                		this.isRounded = true;
                	}
                } else {
                    System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value - convert|c: farenheit/celsius - round|r: round/unround");
                }
            } catch (SensorNotActivatedException ex) {
                Logger.getLogger(ConsoleUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
