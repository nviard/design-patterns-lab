package eu.telecomnancy.sensor;


public class TemperatureSensor extends AbstractSensor {
    double value = 0;
    
    SensorState stateOn;
    SensorState stateOff;
    
    SensorState state;
    
    public TemperatureSensor(){
    	this.stateOn = new SensorOn(this);
    	this.stateOff = new SensorOff(this);
    	this.state = this.stateOff;
    }

    @Override
    public void on() {
        this.state.handleOn();
    }

    @Override
    public void off() {
       this.state.handleOff();
    }

    @Override
    public boolean getStatus() {
        return this.state.handleGetStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
    	this.state.handleUpdate();
    	this.setChanged();
    	this.notifyObservers();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return this.state.handleGetValue();
    }

}