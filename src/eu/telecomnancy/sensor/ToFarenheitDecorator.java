package eu.telecomnancy.sensor;

public class ToFarenheitDecorator extends ISensorDecorator {

	public ToFarenheitDecorator(ISensor component) {
		super(component);
	}
	
	public double getValue() throws SensorNotActivatedException {
		return super.getValue() * 1.8 + 32;
	}

}
