package eu.telecomnancy.sensor;

public abstract class ISensorDecorator implements ISensor {

	ISensor component;
	
	public ISensorDecorator(ISensor component){
		this.component = component;
	}
	
	@Override
	public void on() {
		this.component.on();
	}

	@Override
	public void off() {
		this.component.off();
	}

	@Override
	public boolean getStatus() {
		return this.component.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.component.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.component.getValue();
	}

}
