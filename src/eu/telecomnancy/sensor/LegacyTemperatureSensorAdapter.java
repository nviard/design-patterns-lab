package eu.telecomnancy.sensor;

public class LegacyTemperatureSensorAdapter extends AbstractSensor {
	
	private LegacyTemperatureSensor adaptee;
	private double temperature = 0.0;
	
	public LegacyTemperatureSensorAdapter(LegacyTemperatureSensor adaptee) {
		this.adaptee = adaptee;
	}
	
	@Override
	public void on() {
		if(!this.getStatus())
			this.adaptee.onOff();
	}

	@Override
	public void off() {
		if(this.getStatus())
			this.adaptee.onOff();
	}

	@Override
	public boolean getStatus() {
		return this.adaptee.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if(!this.getStatus())
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
		this.temperature = this.adaptee.getTemperature();
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if(!this.getStatus())
			throw new SensorNotActivatedException("Sensor must be activated to get its value.");
		return this.temperature;
	}

}
