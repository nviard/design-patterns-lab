package eu.telecomnancy.sensor;

public interface SensorState {
	public void handleUpdate() throws SensorNotActivatedException;
	public double handleGetValue() throws SensorNotActivatedException;
	public boolean handleGetStatus();
	public void handleOff();
	public void handleOn();
}
