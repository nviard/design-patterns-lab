package eu.telecomnancy.sensor;

public class SensorOff implements SensorState {
	public TemperatureSensor sensor;
	public SensorOff(TemperatureSensor sensor){
		this.sensor = sensor;
	}
	public void handleUpdate() throws SensorNotActivatedException{
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}
	public double handleGetValue() throws SensorNotActivatedException{
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
	@Override
	public boolean handleGetStatus() {
		return false;
	}
	@Override
	public void handleOff() {}
	@Override
	public void handleOn() {
		this.sensor.state = this.sensor.stateOn;
	}
}
