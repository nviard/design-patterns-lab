package eu.telecomnancy.sensor;

import java.util.Random;

public class SensorOn implements SensorState {
	public TemperatureSensor sensor;
	public SensorOn(TemperatureSensor sensor){
		this.sensor = sensor;
	}
	public void handleUpdate() throws SensorNotActivatedException {
		this.sensor.value = (new Random()).nextDouble() * 100;
	}
	public double handleGetValue() throws SensorNotActivatedException {
		return this.sensor.value;
	}
	@Override
	public boolean handleGetStatus() {
		return true;
	}
	@Override
	public void handleOn() {}
	@Override
	public void handleOff() {
		this.sensor.state = this.sensor.stateOff;
	}
}
