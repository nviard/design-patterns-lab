package eu.telecomnancy.sensor;

public class RoundDecorator extends ISensorDecorator {
	public RoundDecorator(ISensor component) {
		super(component);
	}
	
	public double getValue() throws SensorNotActivatedException {
		return Math.round(super.getValue());
	}
}
