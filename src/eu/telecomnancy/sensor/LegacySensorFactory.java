package eu.telecomnancy.sensor;

public class LegacySensorFactory extends SensorFactory {

	@Override
	public ISensor getSensor() {
		return new LegacyTemperatureSensorAdapter(new LegacyTemperatureSensor());
	}

}
