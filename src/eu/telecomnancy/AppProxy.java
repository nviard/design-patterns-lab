package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.ui.ConsoleUI;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:18
 */
public class AppProxy {
    public static void main(String[] args) {
        ISensor sensor = SensorFactory.makeSensor("Proxy");
        new ConsoleUI(sensor);
    }
}
