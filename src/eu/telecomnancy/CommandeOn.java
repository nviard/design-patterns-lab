package eu.telecomnancy;

import eu.telecomnancy.sensor.AbstractSensor;

public class CommandeOn implements Command {
	private AbstractSensor sensor;

	public CommandeOn(AbstractSensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void execute() {
		sensor.on();
	}
}
