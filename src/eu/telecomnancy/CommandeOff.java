package eu.telecomnancy;

import eu.telecomnancy.sensor.AbstractSensor;

public class CommandeOff implements Command {
	private AbstractSensor sensor;

	public CommandeOff(AbstractSensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void execute() {
		sensor.off();
	}
	
}
