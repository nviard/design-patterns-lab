package eu.telecomnancy;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeUpdate implements Command{
	private AbstractSensor sensor;

	public CommandeUpdate(AbstractSensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void execute() {
		try {
            sensor.update();
        } catch (SensorNotActivatedException sensorNotActivatedException) {
            sensorNotActivatedException.printStackTrace();
        }
	}
}
